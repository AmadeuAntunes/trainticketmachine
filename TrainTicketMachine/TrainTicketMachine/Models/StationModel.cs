﻿namespace TrainTicketMachine.Models
{
    //This class represent only the model of a station 
    //It was not required just done to exemplify O.O.P
    public class StationModel
    {
        public int StationId { get; set; }
        public string StationName { get; set; }

        public StationModel(int StationId, string StationName)
        {
            this.StationId = StationId;
            this.StationName = StationName;
        }

    }
   
}
