﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainTicketMachine.Models;

namespace TrainTicketMachine.Source
{
    public class StationsList
    {
        //This list is readonly cause there is no need change this list

        public List<StationModel> StationList = new List<StationModel>
        {
            new StationModel(1, "Dartford"),
            new StationModel(2, "Dartmouth"),
            new StationModel(3,"Londres"),
            new StationModel(4,"Liverpool"),
            new StationModel(5,"Manchester"),
            new StationModel(6,"Leeds"),
            new StationModel(7,"Sheffield"),
            new StationModel(8,"York"),
            new StationModel(9,"Southampton"),
            new StationModel(10,"Birmingham"),
            new StationModel(11, "Oxford"),
            new StationModel(12, "Canterbury"),
            new StationModel(13, "Carlisle"),
            new StationModel(14, "Cambridge"),
            new StationModel(15, "Gloucester")
        };

       
       
    }
}
