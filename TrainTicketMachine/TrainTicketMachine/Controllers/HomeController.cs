﻿namespace TrainTicketMachine.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using System.Diagnostics;
    using System.Linq;
    using TrainTicketMachine.Models;
    using TrainTicketMachine.Services;
    using TrainTicketMachine.Source;

    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            
            return View();
        }

        [HttpPost]
        public IActionResult Index(string Station)
        {
            //Select only the stationName in the Source
            var StationList = new StationsList().StationList.Select(x => x.StationName);
            var service = new Service();
            var content = string.Empty;
            foreach (var item in service.Search(Station, StationList))
            {
                content = content + "<a>" + item + "</a><br/>";
            } 

            return Content(content);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
