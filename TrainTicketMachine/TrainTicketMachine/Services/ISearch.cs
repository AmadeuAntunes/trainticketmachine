﻿namespace TrainTicketMachine.Services
{
  
    using System.Collections.Generic;
    //This interface is responsible for Search algorithm
    interface ISearch
    {
        
        IEnumerable<string> Search(string word, IEnumerable<string> listSource);

        IEnumerable<string> SearchNextCharacter(string word, IEnumerable<string> listSource);
    }
}
