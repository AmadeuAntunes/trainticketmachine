﻿namespace TrainTicketMachine.Services
{
    using System.Collections.Generic;
    using System.Linq;

    public class Service : ISearch
    {
        /// <summary>
        /// Search in a IEnumerable and return all Itens Start With word Parameter
        /// </summary>
        /// <param name="word">Word to be search</param>
        /// <param name="listSource">Source of search</param>
        /// <returns></returns>
        public  IEnumerable<string> Search(string word, IEnumerable<string> listSource)
        {
            var UpdatedList = from search in listSource
                              where search.ToLower().StartsWith(word.ToLower())
                              select search;
           
            return UpdatedList;
        }

        /// <summary>
        /// Search for next Character
        /// </summary>
        /// <param name="word">Word to be search</param>
        /// <param name="listSource">Source of search</param>
        /// <returns></returns>
        public IEnumerable<string> SearchNextCharacter(string word, IEnumerable<string> listSource)
        {
            var UpdatedList = from search in listSource
                              where search.ToLower().StartsWith(word.ToLower())
                              select search;


            List<string> letters = new List<string>();
            foreach (var station in UpdatedList)
            {
                if (station.Length - 1 > word.Length)
                    letters.Add(station[word.Length].ToString());
            }
            return letters;
        }
    }
}
