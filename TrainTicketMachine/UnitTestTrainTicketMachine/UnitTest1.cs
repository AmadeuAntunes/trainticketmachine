namespace UnitTestTrainTicketMachine
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Collections.Generic;
    using System.Linq;
    using TrainTicketMachine.Services;
    using TrainTicketMachine.Source;

    [TestClass]
    public class UnitTest1
    {


        [TestMethod]
        public void FirstScenery()
        {
           
            List<string> ListStations = new List<string>{ "DARTFORD", "DARTMOUTH", "TOWER HILL", "DERBY" };
            Service Search = new Service();
            var SearchedLetters = Search.SearchNextCharacter("DART", ListStations);
            Assert.IsTrue(SearchedLetters.Contains("F"));
            Assert.IsTrue(SearchedLetters.Contains("M"));
            var ListSearch = Search.Search("DART", ListStations);
            Assert.IsTrue(ListSearch.Contains("DARTFORD"));
            Assert.IsTrue(ListSearch.Contains("DARTMOUTH"));

        }


        [TestMethod]
        public void SecondScenery()
        {
            List<string> ListStations = new List<string> { "LIVERPOOL", "LIVERPOOL LIME STREET", "PADDINGTON" };
            Service Search = new Service();
            var SearchedLetters = Search.SearchNextCharacter("LIVERPOOL", ListStations);
            Assert.IsTrue(SearchedLetters.Contains(" "));
            var ListSearch = Search.Search("LIVERPOOL", ListStations);
            Assert.IsTrue(ListSearch.Contains("LIVERPOOL"));
            Assert.IsTrue(ListSearch.Contains("LIVERPOOL LIME STREET"));

        }


        [TestMethod]
        public void ThirdScenery()
        {
            List<string> ListStations = new List<string> { "EUSTON", "LONDON BRIDGE", "VICTORIA" };
            Service Search = new Service();
            var SearchedLetters = Search.SearchNextCharacter("KINGS CROSS", ListStations);
            Assert.AreEqual(SearchedLetters.Count(), 0);

            var ListSearch = Search.Search("KINGS CROSS", ListStations);
            Assert.AreEqual(ListSearch.Count(), 0);
           

        }





    }
}
